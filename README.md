# Acesso ao GKE para Desenvolvedores

## Configuração Inicial

1. crie uma conta google, no formato `nome.sobrenome.cedro@gmail.com`
2. Informe seu e-mail aos adms e aceite o convite para participar do projeto quando ele chegar ao seu e-mail.
3. Instale o gcloud: <https://cloud.google.com/sdk/docs/install>
4. Instale o Lens IDE: <https://k8slens.dev/>

## Uso

### Windows

No Windows, o gcloud instala um shell chamado `Google Cloud SDK Shell`.
o comando é acessível no terminal CMD. Dependendo da versão do Powershell, a execução de scripts estará bloqueada e isso vai impedir o gcloud de funcionar.

Você pode driblar esse bloqueio e habilitar o gcloud de forma manual, executando o powershell com os seguintes argumentos:

```bat
powershell.exe -NoExit -ExecutionPolicy Bypass -File "%LOCALAPPDATA%\Google\Cloud SDK\google-cloud-sdk\platform\PowerShell\GoogleCloud\BootstrapCloudToolsForPowerShell.ps1"
```

Também é possível desbloquear permanentemente a execução de scripts, se achar melhor:

```powershell
Set-ExecutionPolicy -ExecutionPolicy Unrestricted
```

1. Autentique-se no Google Cloud: `gcloud auth login`
2. Clique em permitir na aba do navegador
    ![tela de login do google](./images/login.png)
3. Instale o kubectl se ainda não possuir. Pode ser com o comando `gcloud components install kubectl`
4. Obtenha id do projeto, nome do cluster e região do cluster. Pode ser na [janela de cluster do console do Google Cloud](https://console.cloud.google.com/kubernetes/list) ou consultando alguém que saiba.
5. Gere as credenciais para acessar o cluster kubernetes: `gcloud container clusters get-credentials <cluster name> --region <region> --project <project id>`
6. Abra o Lens. Vá em File > Add Cluster. Aponte o Lens para o arquivo config dentro da pasta .kube no seu SO, escolha o contexto de acordo com o cluster que irá trabalhar e aperte o botão `add cluster`.
    ![tela de adição de cluster no Lens IDE](./images/LensIDE.png)
7. Aguarde a conexão e está tudo pronto.
